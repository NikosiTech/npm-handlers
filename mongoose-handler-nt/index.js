// BY NIKOSITECH
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

/**
 *
 * @param {Object} config : {USERNAME, PASSWORD, HOST (required), PORT, DB_NAME(required)}
 * @return {Promise<any>}
 */
function connectDB(config)
{
  return new Promise(async (resolve, reject) =>
  {
    if (config)
    {
      let connectionString = "mongodb://";
      if (config.USERNAME)
        connectionString += config.USERNAME;
      if (config.PASSWORD)
        connectionString += ":"+config.PASSWORD+"@";
      if (config.HOST)
        connectionString += config.HOST;
      if (config.PORT)
        connectionString += ":"+config.PORT;
      if (config.DB_NAME)
        connectionString += "/"+config.DB_NAME;

    mongoose.connect(connectionString, {})
    .then(() =>
    {
      resolve();
    })
    .catch(err =>
    {
      reject(err);
    });
    }
  });
}


module.exports =
{
  connectDB
};