const winston = require('winston');
const appRootPath = require('app-root-path');

const options =
{
  fileProd:
  {
    level: 'info',
    filename: appRootPath+'/logs/app.log',
    format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
    maxsize: 5242880, // 5MB
    maxFiles: 5
  },

  consoleVerbose:
  {
    level: 'verbose',
    handleExceptions: true,
    format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
    colorize: true
  },

  consoleDebug:
  {
    level: 'debug',
    handleExceptions: true,
    format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
    colorize: true
  },
};

// PROD LOGGER
winston.loggers.add('production',
{
  transports: [new winston.transports.File(options.fileProd)],
  exitOnError: false
});

// DEV LOGGER
winston.loggers.add('dev',
{
  transports: [new winston.transports.Console(options.consoleVerbose)],
  exitOnError: false
});


winston.loggers.add('debug',
{
  transports: [new winston.transports.Console(options.consoleDebug)],
  exitOnError: false
});



module.exports = winston.loggers;